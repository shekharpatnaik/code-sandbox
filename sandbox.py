from kubernetes import client, config
from kubernetes.client.api import core_v1_api
from kubernetes.client import Configuration
from kubernetes.stream import stream
import base64
import time

class Sandbox():

    name: str
    image: str
    api_client: core_v1_api.CoreV1Api

    def __init__(self, name: str, image: str = "python:3.11-bullseye"):
        self.name = name
        self.image = image
        config.load_kube_config()
        try:
            c = Configuration().get_default_copy()
        except AttributeError:
            c = Configuration()
            c.assert_hostname = False
        Configuration.set_default(c)
        self.api_client = core_v1_api.CoreV1Api()

    def start(self):
        # self.api_client.create_namespace(
        #     body=client.V1Namespace(
        #         metadata=client.V1ObjectMeta(name=self.name)
        #     )
        # )

        pod = client.V1Pod()
        pod.metadata = client.V1ObjectMeta(name=self.name)

        pod.spec = client.V1PodSpec(
            containers=[
                client.V1Container(
                    name="runtime",
                    image=self.image,
                    command=['sleep', 'infinity'],
                    volume_mounts=[
                        client.V1VolumeMount(
                            name="ssh",
                            mount_path="/root/.ssh"
                        )
                    ],
                    env= [
                        client.V1EnvVar(
                            name="GIT_SSH_COMMAND",
                            value="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"
                        )
                    ]
                )
            ],
            volumes= [
                client.V1Volume(
                    name="ssh",
                    secret=client.V1SecretVolumeSource(
                        secret_name="ssh-key",
                        default_mode=0o600
                    )
                )
            ]
        )

        self.api_client.create_namespaced_pod(
            body=pod,
            namespace='default'
        )

        # Wait for the pod to start running
        while True:
            pod = self.api_client.read_namespaced_pod(name=self.name,
                                                    namespace='default')
            if pod.status.phase != 'Pending':
                break
            time.sleep(1)

        self.exec_resp = stream(self.api_client.connect_get_namespaced_pod_exec,
            name=self.name,
            namespace='default',
            command=["/bin/sh"],
            stderr=True,
            stdin=True,
            stdout=True,
            tty=False,
            _preload_content=False
        )

    def run_command(self, command: str, timeout: int = 1):
        ran_command = False
        output = ""
        while self.exec_resp.is_open():
            self.exec_resp.update(timeout=timeout)
            if self.exec_resp.peek_stdout():
                output += self.exec_resp.read_stdout()
            if self.exec_resp.peek_stderr():
                output += self.exec_resp.read_stderr()
            if not(ran_command):
                print(f"Running command... {command}\n")
                self.exec_resp.write_stdin(command + "\n")
                ran_command = True
            else:
                break

        return output
    
    def stop(self):
        self.api_client.delete_namespaced_pod(name=self.name,
                                            namespace='default')

    def read_file(self, file_path: str) -> str:
        return self.run_command(f"cat {file_path}")
    
    def write_file(self, file_path: str, file_contents: str) -> str:
        encoded_contents = base64.b64encode(file_contents.encode('utf-8')).decode('utf-8')
        return self.run_command(f'echo "{encoded_contents}" | base64 -d > {file_path}')