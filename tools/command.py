from langchain.tools import BaseTool
from langchain.pydantic_v1 import BaseModel, Field
from typing import Type

class RunCommandInput(BaseModel):
    command: str = Field(description="The command to run")

class RunCommand(BaseTool):
    name = "run_command"
    description = """Run a command in the repository working directory."""
    args_schema: Type[BaseModel] = RunCommandInput

    def _run(self, command: str) -> str:
        # Change to workspace directory while keeping the original working directory
        sandbox = self.metadata['sandbox']
        return sandbox.run_command(command)