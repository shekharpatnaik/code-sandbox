from langchain.tools import BaseTool
from langchain.pydantic_v1 import BaseModel, Field
from typing import Type

class CloneInput(BaseModel):
    repository_url: str = Field(description="the url for the repository to clone")

class Clone(BaseTool):
    name = "clone_repository"
    description = "Clone a Git repository."
    args_schema: Type[BaseModel] = CloneInput

    def _run(self, repository_url: str) -> str:
        """Use the tool."""
        sandbox = self.metadata['sandbox']
        result = sandbox.run_command(f"git clone {repository_url} /workspace")
        sandbox.run_command("cd /workspace")
        return result
        

class CreateBranchCommitPushInput(BaseModel):
    branch_name: str = Field(description="a branch name specific to the issue. please use best pratices for naming")
    commit_message: str = Field(description="A description commit message describing the change.")

class CreateBranchCommitPush(BaseTool):
    name = "create_branch_commit_and_push"
    description = """Create a branch with specified name, add files, commit and push the changes to the repository.
    The branch name and commit_message should be descriptive and should contain information about
    the fix as well as the issue number. Please run this after the issue is fixed."""
    args_schema: Type[BaseModel] = CreateBranchCommitPushInput

    def _run(self, branch_name: str, commit_message: str) -> str:
        sandbox = self.metadata['sandbox']
        sandbox.run_command(f"git checkout -b {branch_name}")
        sandbox.run_command(f"git add .")
        sandbox.run_command(f"git commit -m '{commit_message}'")
        sandbox.run_command(f"git push origin {branch_name}")
        return "SUCCESS"